flatpak-builder --user --install --force-clean build-dir build.json

http://supervisord.org/configuration.html
https://pypi.org/project/supervisor/
https://docs.flatpak.org/en/latest/python.html

export CWD=~/.config/supervisor
mkdir -p $CWD
echo_supervisord_conf > $CWD/supervisord.conf

{
    "name": "importlib-metadata",
    "buildsystem": "simple",
    "build-commands": [
        "pip install importlib-metadata -i https://pypi.tuna.tsinghua.edu.cn/simple --prefix=/app"
    ]
},

- 配置

```conf
[inet_http_server]
port=127.0.0.1:9001 ;IP按需配置
username=user
password=123
```

supervisorctl --help